local class = require("lib.30log")

local ecs = {}

ecs.world = class("world")

function ecs.world:init()
  self.entitiesCount = 0
  self.vacantIDs = {}
  self.entities = {}
  self.systems = {}
  self.resources = {}
end

function ecs.world:addEntity(entity)
  self.entitiesCount = self.entitiesCount + 1

  local id = next(self.vacantIDs) or (#self.entities + 1)

  entity.id = id
  self.entities[id] = entity
  self.vacantIDs[id] = nil

  return id
end

function ecs.world:removeEntity(id)
  self.entities[id] = nil
  self.vacantIDs[id] = true
  self.entitiesCount = self.entitiesCount - 1
end

function ecs.world:getEntity(id)
  return self.entities[id]
end

function ecs.world:addSystem(name, system)
  self.systems[name] = system
end

function ecs.world:getSystem(name)
  return self.systems[name]
end

function ecs.world:addResource(name, value)
  self.resources[name] = value
end

function ecs.world:getResource(name)
  return self.resources[name]
end

function ecs.world:exec(name, method, ...)
  local system = self.systems[name]

  for _, entity in pairs(self.entities) do
    if system.filter(entity) then
      system[method](system, self, entity, ...)
    end
  end
end

function ecs.world:fire(method, ...)
  for name, system in pairs(self.systems) do
    if system[method] then
      self:exec(name, method, ...)
    end
  end
end

function ecs.world:select(filter)
  return coroutine.wrap(function()
    for _, entity in pairs(self.entities) do
      if filter(entity) then
        coroutine.yield(entity)
      end
    end
  end)
end

function ecs.requireAll(...)
  local components = {...}
  return function(entity)
    for _, component in ipairs(components) do
      if not entity[component] then
        return false
      end
    end

    return true
  end
end

return ecs
