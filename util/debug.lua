local class = require("lib.30log")
local assets = require("assets")

local debug = {}

debug.graph = class("debug_graph")

function debug.graph:init(x, y, label, fmt)
  self.x = x
  self.y = y
  self.label = label
  self.fmt = fmt
  self.delay = 0.5
  self.limit = 150
  self.timePassed = math.huge
  self.values = {}
end

function debug.graph:update(dt, val)
  self.timePassed = self.timePassed + dt

  if self.timePassed < self.delay then
    return
  end

  self.timePassed = 0

  if #self.values == self.limit then
    table.remove(self.values, 1)
  end

  table.insert(self.values, val)
end

function debug.graph:draw()
  love.graphics.push()
  love.graphics.translate(self.x, self.y)


  local min, max = math.huge, 0

  for _, val in ipairs(self.values) do
    min = math.min(min, val)
    max = math.max(max, val)
  end

  local val = self.values[#self.values] or 0

  love.graphics.setColor(0.3, 0.3, 0.3)
  love.graphics.setFont(assets.fonts.debug)
  love.graphics.print((self.label .. ": " .. self.fmt):format(val), 0, 30)
  love.graphics.setFont(assets.fonts.debug_tiny)
  love.graphics.print(("min: " .. self.fmt .. ", max: " .. self.fmt):format(min, max), 0, 46)

  if min == max then
    min = min - 1
  end

  love.graphics.setColor(0, 0, 1, 0.5)
  local oldy = 0
  for x, val in ipairs(self.values) do
    local y = 30 - (val - min) / (max - min) * 30

    if x > 1 then
      love.graphics.line(x - 1, oldy, x, y)
    end

    oldy = y
  end

  love.graphics.pop()
end

return debug
