local ffi

local aabb = {}

if jit and jit.status() then
  ffi = require("ffi")
  ffi.cdef[[
  typedef struct {
    double x1;
    double y1;
    double x2;
    double y2;
  } aabb;
  ]]
end

function aabb.move(aabb, delta)
  aabb.x1 = aabb.x1 + delta.x
  aabb.x2 = aabb.x2 + delta.x
  aabb.y1 = aabb.y1 + delta.y
  aabb.y2 = aabb.y2 + delta.y
end

function aabb.test(lhs, rhs)
  return lhs.x2 >= rhs.x1
     and lhs.x1 <= rhs.x2
     and lhs.y2 >= rhs.y1
     and lhs.y1 <= rhs.y2
end

if ffi then
  function aabb.__call(_, x1, y1, x2, y2)
    return ffi.new("aabb", x1, y1, x2, y2)
  end
else
  function aabb.__call(_, x1, y1, x2, y2)
    return setmetatable({x1 = x1, y1 = y1, x2 = x2, y2 = y2}, aabb)
  end
end

if ffi then
  ffi.metatype("aabb", aabb)
end

setmetatable(aabb, aabb)

return aabb
