local class = require("lib.30log")
local vec = require("lib.brinevector")

local camera = class("camera")

function camera:init(scale)
  self.pos = vec(0, 0)
  self.scale = scale
end

function camera:move(v)
  self.pos = self.pos + v
end

function camera:setPosition(v)
  self.pos = v
end

function camera:getPosition()
  return self.pos
end

function camera:toWorld(pos)
  local width, height = love.graphics.getDimensions()
  return pos / self.scale + self.pos - vec(width, height) / 2 / self.scale
end

function camera:toScreen(pos)
  return pos * self.scale - self.pos
end

function camera:apply()
  local width, height = love.graphics.getDimensions()
  love.graphics.push()
  love.graphics.translate(width / 2, height / 2)
  love.graphics.scale(self.scale)
  love.graphics.translate(-self.pos.x, -self.pos.y)
end

function camera:unapply()
  love.graphics.pop()
end

function camera:draw(f)
  self:apply()
  f(self)
  self:unapply()
end

return camera
