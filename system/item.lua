local class = require("lib.30log")
local vec = require("lib.brinevector")
local ecs = require("ecs")
local assets = require("assets")

local item_system = class("item_system")
item_system.filter = ecs.requireAll("item", "position", "lifetime", "age", "params", "aabb")
local pfilter

function item_system:update(world, entity, dt)
  if not pfilter then
    pfilter=world:getSystem("player").filter
  end
  
  entity.age = entity.age + dt
  if entity.age > entity.lifetime then
    world:addEntity({
      particle = "raising_image",
      position = entity.position,
      lifetime = 0.5,
      age = 0,
      params = {
        imageScale_start = 1,
        imageScale_final = 2,
        imageColor_start = {1, 1, 1, 1},
        imageColor_final = {1, 1, 1, 0},
        image = entity.params.image
      }
    })
    world:removeEntity(entity.id)
    return
  end
  
  world:getSystem("collision"):collisionTest(world, entity)
  
  if entity.collisions then
    for other in pairs(entity.collisions) do
      if pfilter(other) then
        entity.params.onPickup(entity,other)
        world:addEntity({
          particle = "pickup",
          position = entity.position,
          lifetime = 0.2,
          age = 0,
          params = {
            imageScale_start = 1,
            imageScale_final = 0,
            imageColor_start = {1, 1, 1, 1},
            imageColor_final = {1, 1, 1, 1},
            image = entity.params.image,
            target = other
          }
        })
        world:removeEntity(entity.id)
      end
    end
  end
end

function item_system:drawItems(world, entity)
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(entity.params.image, entity.position.x, entity.position.y, 0, 1, 1, entity.params.image:getWidth()/2, entity.params.image:getHeight()/2)
end

return item_system
