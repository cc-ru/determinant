local class = require("lib.30log")
local vec = require("lib.brinevector")
local ecs = require("ecs")
local assets = require("assets")
local aabb = require("util.aabb")

local particle_system = class("particle_system")
particle_system.filter = ecs.requireAll("particle","position","lifetime","age","params")
local function sign(x)
  return x > 0 and (1) or (x < 0 and -1 or 0)
end
function particle_system:update(world, entity, dt, isPart)
  if entity.particle == "bunch" then
    for k,v in pairs(entity.params.particles) do
      local b = self:update(world, v, dt, true)
      if not b then
        entity.params.particles[k] = nil
      end
    end
    if not next(entity.params.particles) and not entity.save then
      if isPart then
        return false
      else
        entity.isDead = true
        world:removeEntity(entity.id)
      end
    end
    if isPart then
      return true
    end
  else
    entity.age=entity.age+dt
    if entity.velocityParent then
      entity.velocity = entity.velocityParent.velocity
    end
    local shift = vec(0,0)
    local steps = 0.2
    entity.stepsPast = (entity.stepsPast or 0)+steps
    --for fr=1,steps do
    while entity.stepsPast >= 1 do
      entity.stepsPast = entity.stepsPast - 1
      if entity.velocity then
        entity.position = entity.position + (entity.velocity*(dt/steps))
        if entity.aabb then
          aabb.move(entity.aabb, entity.velocity*(dt/steps))
        end
      end
      
      if entity.aabb then
        local eshift = vec(0, 0)
        world:getSystem("collision"):collisionTest(world, entity)
        if entity.collisions and entity.collision then
          for other, b in pairs(entity.collisions) do
            if world:getSystem("wall").filter(other) then
              local wpos = vec((other.aabb.x1 + other.aabb.x2) / 2, (other.aabb.y1 + other.aabb.y2) / 2)
              local epos = vec((entity.aabb.x1 + entity.aabb.x2) / 2, (entity.aabb.y1 + entity.aabb.y2) / 2) + eshift
              local wsize = vec(math.abs(other.aabb.x1 - other.aabb.x2), math.abs(other.aabb.y1 - other.aabb.y2))
              local esize = vec(math.abs(entity.aabb.x1 - entity.aabb.x2), math.abs(entity.aabb.y1 - entity.aabb.y2))
              local dist_vec = epos - wpos
              local needed_dist = vec((wsize.x + esize.x) * sign(dist_vec.x), (wsize.y + esize.y) * sign(dist_vec.y)) / 2
              if needed_dist.length > dist_vec.length then
                if math.abs(dist_vec.x) < math.abs(needed_dist.x) and math.abs(dist_vec.y) < math.abs(needed_dist.y) then
                  if math.max(math.abs(dist_vec.x), math.abs(dist_vec.y)) == math.abs(dist_vec.x) then
                    eshift = eshift + vec(needed_dist.x - dist_vec.x, 0)
                    entity.velocity.x = -entity.velocity.x
                  else
                    eshift = eshift + vec(0, needed_dist.y - dist_vec.y)
                    entity.velocity.y = -entity.velocity.y
                  end
                end
              end
            end
          end
        end

        entity.position = entity.position + (eshift)
        if entity.aabb then
          aabb.move(entity.aabb, eshift)
        end
      end
    end

    if entity.age > entity.lifetime then
      if isPart then
        return false
      else
        world:removeEntity(entity.id)
      end
    end
    if isPart then
      return true
    end
  end
end
function particle_system:drawParticles(world, entity, renderLayer)
  local t = entity.age/entity.lifetime
  if entity.renderLayer ~= renderLayer and not (entity.particle == "bunch") then
    return
  end
  if entity.particle == "raising_circle" then
    local rW_s = entity.params.ringWidth_start
    local rW_f = entity.params.ringWidth_final
    local rC_s = entity.params.ringColor_start
    local rC_f = entity.params.ringColor_final
    local cW_s = entity.params.circleRadius_start
    local cW_f = entity.params.circleRadius_final

    love.graphics.push("all")
    love.graphics.setLineWidth(rW_s + (rW_f - rW_s) * t)
    love.graphics.setColor(
      rC_s[1] + (rC_f[1] - rC_s[1]) * t,
      rC_s[2] + (rC_f[2] - rC_s[2]) * t,
      rC_s[3] + (rC_f[3] - rC_s[3]) * t,
      rC_s[4] + (rC_f[4] - rC_s[4]) * t
    )
    
    love.graphics.circle("line", entity.position.x, entity.position.y, cW_s + (cW_f - cW_s) * t)
    
    love.graphics.pop()
    
  elseif entity.particle == "raising_image" then
    local iS_s = entity.params.imageScale_start
    local iS_f = entity.params.imageScale_final
    local iC_s = entity.params.imageColor_start
    local iC_f = entity.params.imageColor_final
    local iD = entity.params.imageRotation or 0
    local img = entity.params.image
    local scale = iS_s + (iS_f - iS_s) * t
    
    love.graphics.push("all")
    love.graphics.setColor(
      iC_s[1] + (iC_f[1] - iC_s[1]) * t,
      iC_s[2] + (iC_f[2] - iC_s[2]) * t,
      iC_s[3] + (iC_f[3] - iC_s[3]) * t,
      iC_s[4] + (iC_f[4] - iC_s[4]) * t
    )
    
    love.graphics.draw(img, entity.position.x, entity.position.y, iD, scale, scale, img:getWidth()/2, img:getHeight()/2)
    
    love.graphics.pop()
  elseif entity.particle == "pickup" then
    local iS_s = entity.params.imageScale_start
    local iS_f = entity.params.imageScale_final
    local iC_s = entity.params.imageColor_start
    local iC_f = entity.params.imageColor_final
    local target = entity.params.target
    local img = entity.params.image
    local scale = iS_s + (iS_f - iS_s) * t
    local pos = entity.position + (target.position - entity.position) * t
    
    love.graphics.push("all")
    love.graphics.setColor(
      iC_s[1] + (iC_f[1] - iC_s[1]) * t,
      iC_s[2] + (iC_f[2] - iC_s[2]) * t,
      iC_s[3] + (iC_f[3] - iC_s[3]) * t,
      iC_s[4] + (iC_f[4] - iC_s[4]) * t
    )
    
    love.graphics.draw(img, pos.x, pos.y, 0, scale, scale, img:getWidth()/2, img:getHeight()/2)
    love.graphics.pop()
  elseif entity.particle == "bunch" then
    for k,v in pairs(entity.params.particles) do
      self:drawParticles(world, v, renderLayer)
    end
  elseif entity.particle == "custom" then
    entity.params.drawParticle(entity)
  end
end
return particle_system
