local class = require("lib.30log")
local vec = require("lib.brinevector")
local aabb = require("util.aabb")
local ecs = require("ecs")
local assets = require("assets")

local enemy_system = class("enemy_system")
enemy_system.filter = ecs.requireAll("enemy", "position", "hp", "aabb", "speed", "direction")
local pfilter
local function sign(x)
  return x > 0 and (1) or (x < 0 and -1 or 0)
end
local function death_particle(world, pos, age)
  local t = {
    particle = "bunch",
    position = vec(0,0),
    lifetime = -1,
    age = -1,
    save = false,
    params = {
      particles = {}
    }
  }
  local function add(e)
    table.insert(t.params.particles, e)
  end
  for n=1, 50 do
    add({
      particle = "raising_circle",
      position = pos + (vec(math.random() * 2 - 1, math.random() * 2 - 1).normalized * math.random() * 32),
      lifetime = 0.1 + math.random() * 0.4,
      age = age or 0,
      params = {
        ringWidth_start = 10,
        ringWidth_final = math.random(10, 20),
        circleRadius_start = 10,
        circleRadius_final = math.random(32, 40),
        ringColor_start = math.random(2) == 1 and 
        {math.random() * 0.5 + 0.5, math.random() * 0.6, math.random() * 0.01, 1} or
        {math.random() * 0.5 + 0.5, math.random() * 0.1, math.random() * 0.01, 1},
        ringColor_final = {math.random() * 0.1 + 0.1, math.random() * 0.15, math.random() * 0.001, 0}
      }
    })
  end
  world:addEntity(t)
end
local function bite_particle(world, pos, age)
  local t = {
    particle = "bunch",
    position = vec(0,0),
    lifetime = -1,
    age = -1,
    save = false,
    params = {
      particles = {}
    }
  }
  local function add(e)
    table.insert(t.params.particles, e)
  end
  for n=1, 50 do
    add({
      particle = "raising_circle",
      position = pos + (vec(math.random() * 2 - 1, math.random() * 2 - 1).normalized * math.random() * 64),
      lifetime = 0.1 + math.random() * 0.4,
      age = age or 0,
      params = {
        ringWidth_start = 10,
        ringWidth_final = 30,
        circleRadius_start = 10,
        circleRadius_final = math.random(40, 50),
        ringColor_start = {math.random() * 0.5 + 0.5, math.random() * 0.25, math.random() * 0.05, 1},
        ringColor_final = {1, 0, 0, 0}
      }
    })
  end
  world:addEntity(t)
end
function enemy_system:update(world, entity, dt)
  if entity.hp < entity.maxhp then
    entity.hp_bar_transparency = 1
  else
    entity.hp_bar_transparency = math.max((entity.hp_bar_transparency or 0) - dt * 0.5, 0)
  end
  if entity.hp <= 0 then
    death_particle(world, entity.position, age)
    -- Враг умер, спавним дроп
    local n = math.random(4) -- Рандомом выбираем, какой будет дроп
    local onPickup, image
    if entity.onDeath then
      entity.onDeath(world, entity)
    end
    if n == 1 then -- Отскакивание пуль
      onPickup = function(item, player)
        for _,weapon in pairs(player.weapons) do
          weapon.fireLoss = weapon.fireLoss / 2
        end
      end
      image = assets.images.items.fireBounceUpgrade
    elseif n == 2 then -- Усиление урона от пуль
      onPickup = function(item, player)
        for _, weapon in pairs(player.weapons) do
          weapon.firePower = weapon.firePower * 1.1
          weapon.fireLoss = weapon.fireLoss * 1.1
        end
      end
      image = assets.images.items.firePowerUpgrade
    elseif n == 3 then -- Скорострельность
      onPickup = function(item, player)
        for _,weapon in pairs(player.weapons) do
          if weapon.fireRate > 0.05 then -- Защита от лагов. Слишком много пуль нам не надо
            weapon.fireRate = weapon.fireRate / 1.1
          else -- Поэтому, скорострельность слишком большая, мы её не трогаем. Вместо этого увеличиваем силу пуль
            weapon.firePower = weapon.firePower * 1.1
            weapon.fireLoss = weapon.fireLoss * 1.1
            weapon.feedback = weapon.feedback * 1.1
          end
        end
      end
      image = assets.images.items.fireRateUpgrade
    elseif n == 4 then -- Просто ХП
      onPickup = function(item, player)
        player.hp = math.max(player.hp, math.min(player.hp + 20, player.maxhp))
      end
      image = assets.images.items.hp
    end
    world:removeEntity(entity.id)
    assert(image, tostring(image) .. ": " .. n)
    world:addEntity({
      item = true,
      position = entity.position,
      lifetime = 10,
      age = 0,
      aabb = aabb(entity.position.x - 15, entity.position.y - 15, entity.position.x + 15, entity.position.y + 15),
      params = {
        image = image,
        onPickup = onPickup
      }
    })
    return
  end
  if not pfilter then
    pfilter = world:getSystem("player").filter
  end
  entity.quad = love.graphics.newQuad(0, 0, 128, 128, 896, 128)
  if not entity.target then
    local closest_player
    for player in world:select(pfilter) do
      closest_player=
          (closest_player and (closest_player.position - entity.position).length or math.huge)
            >
          (player.position - entity.position).length
        and
          player
        or
          closest_player
    end
    entity.target = closest_player
  end
  local chase_shift = vec(0, 0)
  if entity.target and (entity.target.position - entity.position).length > 64 then
    local ple = entity.target
    entity.animation = ((entity.animation or 0) + dt * 7) % 6
    local vel = ple.position - entity.position
    entity.direction = vel.angle + math.pi / 2
    chase_shift = vel.normalized * dt * (entity.speed)
  end
  
  local moveSteps = 10

  for n = 1, moveSteps do
    local eshift = vec(0, 0)
    
    aabb.move(entity.aabb, chase_shift * (1 / moveSteps))
    entity.position = entity.position + (chase_shift * (1 / moveSteps))

    world:getSystem("collision"):preCollisionTest(world, entity)
    world:getSystem("collision"):collisionTest(world, entity)
    
    if entity.collisions and entity.collision then
      for other, b in pairs(entity.collisions) do
        if pfilter(other) and other == entity.target then
          world:removeEntity(entity.id)
          bite_particle(world, entity.position)
          other.hp = other.hp - 20
          assets.sfx.hurt:seek(0)
          assets.sfx.hurt:play()
          other.shake = other.shake + 64
          return
        elseif enemy_system.filter(other) then
          local ecount = 0
          for k, v in pairs(other.collisions) do
            if enemy_system.filter(k) then
              ecount = ecount + 1
            end
          end
          local ve = (entity.position - other.position)
          local wantedl
          local normal = ve.normalized
          do
            local u = math.max(math.abs(normal.x), math.abs(normal.y))
            local x,y = 
              normal.x / u,
              normal.y / u
            normal = vec(x, y)
          end
          normal = normal * (math.abs(entity.aabb.x1 - entity.aabb.x2))
          local l = (normal.length - ve.length)
          eshift = eshift + (ve.normalized * l / 2) * math.min(dt * 8 * (ecount * ecount), 1)
        elseif world:getSystem("wall").filter(other) then
          local wpos = vec((other.aabb.x1 + other.aabb.x2) / 2, (other.aabb.y1 + other.aabb.y2) / 2)
          local epos = vec((entity.aabb.x1 + entity.aabb.x2) / 2, (entity.aabb.y1 + entity.aabb.y2) / 2) + eshift
          local wsize = vec(math.abs(other.aabb.x1 - other.aabb.x2), math.abs(other.aabb.y1 - other.aabb.y2))
          local esize = vec(math.abs(entity.aabb.x1 - entity.aabb.x2), math.abs(entity.aabb.y1 - entity.aabb.y2))
          local dist_vec = epos - wpos
          local needed_dist = vec((wsize.x + esize.x) * sign(dist_vec.x), (wsize.y + esize.y) * sign(dist_vec.y)) / 2
          if needed_dist.length > dist_vec.length then
            if math.abs(dist_vec.x) < math.abs(needed_dist.x) and math.abs(dist_vec.y) < math.abs(needed_dist.y) then
              if math.max(math.abs(dist_vec.x), math.abs(dist_vec.y)) == math.abs(dist_vec.x) then
                eshift = eshift + vec(needed_dist.x - dist_vec.x, 0)
                chase_shift.x = 0
              else
                eshift = eshift + vec(0, needed_dist.y - dist_vec.y)
                chase_shift.y = 0
              end
            end
          end
        end
      end
    end
    
    aabb.move(entity.aabb, eshift)
    entity.position = entity.position + eshift
  end
end

function enemy_system:drawEnemies(world, entity, overlays)
  if overlays then
    if entity.hp_bar_transparency then
      love.graphics.setColor(0.8, 0.2, 0.2, entity.hp_bar_transparency)
      love.graphics.rectangle("fill", entity.position.x - 60, entity.position.y - 80 - 10, 120, 10)
      love.graphics.setColor(0.2, 0.8, 0.2, entity.hp_bar_transparency)
      love.graphics.rectangle("fill", entity.position.x - 60, entity.position.y - 80 - 10, 120 * entity.hp / entity.maxhp, 10)
    end
  else
    love.graphics.setColor(1, 1, 1)
    if entity.quad then
      local frame = math.floor((entity.animation or 0) + .5)
      entity.quad:setViewport(128 * frame, 0, 128, 128)
      love.graphics.draw(assets.images.shadow, entity.position.x - 68, entity.position.y - 38)
      love.graphics.draw(assets.images.spider, entity.quad, entity.position.x, entity.position.y, entity.direction, 1, 1, 64, 64)
    end
  end
end

return enemy_system
