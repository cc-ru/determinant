local class = require("lib.30log")
local vec = require("lib.brinevector")
local aabb = require("util.aabb")
local ecs = require("ecs")
local assets = require("assets")

local wall_system = class("wall_system")
wall_system.filter = ecs.requireAll("wall", "aabb", "params")

function wall_system:update(world, entity)
  world:getSystem("collision"):preCollisionTest(world, entity)
end

function wall_system:drawWalls(world, entity)
  if entity.wall == "stretchTexture" then
    local x1, y1, x2, y2 = entity.aabb.x1, entity.aabb.y1, entity.aabb.x2, entity.aabb.y2
    love.graphics.setColor(1, 1, 1, 1)
    if entity.params.quad then
      love.graphics.draw(entity.params.texture, entity.params.quad, math.min(x1, x2), math.min(y1, y2), 0, math.abs(x2 - x1) / entity.params.quad:getWidth(), math.abs(y2 - y1) / entity.params.quad.getHeight())
    else
      love.graphics.draw(entity.params.texture, math.min(x1, x2), math.min(y1, y2), 0, math.abs(x2 - x1) / entity.params.texture:getWidth(), math.abs(y2 - y1) / entity.params.texture:getHeight())
    end
  end
end

return wall_system
