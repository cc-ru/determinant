local class = require("lib.30log")
local vec = require("lib.brinevector")
local aabb = require("util.aabb")
local ecs = require("ecs")
local assets = require("assets")

local bullet_system = class("bullet_system")
bullet_system.filter = ecs.requireAll("bullet", "position", "velocity", "lifetime", "age", "aabb", "power", "loss", "thrower_velocity")
local efilter
local function sign(x)
  return x > 0 and (1) or (x < 0 and -1 or 0)
end
local function bullet_miss_particle(world,pos)
  world:addEntity({
    particle="raising_circle",
    position=pos,
    lifetime=0.3,
    age=0,
    params={
      ringWidth_start=0,
      ringWidth_final=80,
      circleRadius_start=0,
      circleRadius_final=40,
      ringColor_start={0.5,0.5,1,1},
      ringColor_final={1,0.5,0.5,0}
    }
  })
end
local function bullet_fly_particle(pb, world,pos,speed,dt)
  table.insert(pb.params.particles, {
    particle="raising_circle",
    position=pos,
    lifetime=0.2,
    age=0,
    params={
      ringWidth_start=5,
      ringWidth_final=100,
      circleRadius_start=2.5,
      circleRadius_final=50,
      ringColor_start={0,1,1,0.01*speed*(dt*60)},
      ringColor_final={1,0.5,0.5,0}
    }
  })
  table.insert(pb.params.particles, {
    particle="raising_circle",
    renderLayer = "bullet_fly",
    position=pos,
    lifetime=0.2,
    age=0,
    params={
      ringWidth_start=5,
      ringWidth_final=20,
      circleRadius_start=2.5,
      circleRadius_final=10,
      ringColor_start={0,1,1,0.1*speed*(dt*60)},
      ringColor_final={1,0.5,0.5,0}
    }
  })
end
local function bullet_particle(world, pos, transparency)
  world:addEntity({
    particle="raising_circle",
    position=pos,
    lifetime=0.3,
    age=0,
    params={
      ringWidth_start=0,
      ringWidth_final=100,
      circleRadius_start=0,
      circleRadius_final=50,
      ringColor_start={0,1,1,math.min(transparency * 10, 1)},
      ringColor_final={0,0.7,1,0}
    }
  })
end

local pfilter

local function nocollide(other) -- Функция, определяющая с какими сущностями эта сущность не коллайдится
  return bullet_system.filter(other)
end

function bullet_system:update(world, entity, dt)
  entity.age = entity.age + dt
  if entity.age >= entity.lifetime then
    bullet_miss_particle(world,entity.position)
    world:removeEntity(entity.id)
    return
  end
  if not efilter then
    efilter=world:getSystem("enemy").filter
  end
  if not pfilter then
    pfilter=world:getSystem("player").filter
  end
  entity.nocollide=nocollide
  entity.particleBunch = entity.particleBunch or world:getEntity(world:addEntity({
    particle = "bunch",
    position = vec(0,0),
    lifetime = -1,
    age = -1,
    save = false,
    params = {
      particles = {}
    }
  }))
  local moveSteps = 30
  local finalVel = vec(entity.velocity.x, entity.velocity.y)
  local oldPos = vec(entity.position.x, entity.position.y)
  local xinv, yinv = false, false
  for n = 1, moveSteps do
    local eshift = vec(0,0)
    entity.position = entity.position + (dt / moveSteps) * (entity.velocity + entity.thrower_velocity)
    aabb.move(entity.aabb, (dt / moveSteps) * (entity.velocity + entity.thrower_velocity))
    world:getSystem("collision"):collisionTest(world, entity)
    local cpower=entity.power
    if entity.collisions then
      for other, b in pairs(entity.collisions) do
        if efilter(other) and other.hp > 0 and entity.lastentity~=other then
          bullet_particle(world,entity.position, entity.hit_transparency or 1)
          other.hp=other.hp-cpower
          entity.power=entity.power-entity.loss
          entity.lastentity=k
          if entity.power > 0 then
            entity.velocity=vec.rotated(entity.velocity,math.random()*math.pi*2)
            entity.age=0
          else
            world:removeEntity(entity.id)
            return
          end
        elseif world:getSystem("wall").filter(other) then
          local wpos = vec((other.aabb.x1 + other.aabb.x2) / 2, (other.aabb.y1 + other.aabb.y2) / 2)
          local epos = vec((entity.aabb.x1 + entity.aabb.x2) / 2, (entity.aabb.y1 + entity.aabb.y2) / 2) + eshift
          local wsize = vec(math.abs(other.aabb.x1 - other.aabb.x2), math.abs(other.aabb.y1 - other.aabb.y2))
          local esize = vec(math.abs(entity.aabb.x1 - entity.aabb.x2), math.abs(entity.aabb.y1 - entity.aabb.y2))
          local dist_vec = epos - wpos
          local needed_dist = vec((wsize.x + esize.x) * sign(dist_vec.x), (wsize.y + esize.y) * sign(dist_vec.y)) / 2
          if math.abs(dist_vec.x) < math.abs(needed_dist.x) and math.abs(dist_vec.y) < math.abs(needed_dist.y) then
            if math.max(math.abs(dist_vec.x), math.abs(dist_vec.y)) == math.abs(dist_vec.x) then
              eshift = eshift + vec(needed_dist.x - dist_vec.x, 0)
              entity.velocity.x = 0
              entity.thrower_velocity = vec(0, entity.thrower_velocity.y)
              if not xinv then
                finalVel.x = -finalVel.x
                bullet_particle(world,entity.position, entity.hit_transparency or 1)
                xinv = true
                entity.power=entity.power-entity.loss
                if entity.power > 0 then
                  --entity.age=0
                else
                  world:removeEntity(entity.id)
                  return
                end
              end
            else
              eshift = eshift + vec(0, needed_dist.y - dist_vec.y)
              entity.velocity.y = 0
              entity.thrower_velocity = vec(entity.thrower_velocity.y, 0)
              if not yinv then
                finalVel.y = -finalVel.y
                bullet_particle(world,entity.position, entity.hit_transparency or 1)
                yinv = true
                entity.power=entity.power-entity.loss
                if entity.power > 0 then
                  --entity.age=0
                else
                  world:removeEntity(entity.id)
                  return
                end
              end
            end
          end
        end
      end
    end
    entity.position = entity.position + eshift
    aabb.move(entity.aabb, eshift)
  end
  do
    local vel = entity.position - oldPos
    local steps = math.min(10, math.max(2, (vel.length / dt) / 200))
    local cam = world.resources.camera
    local min,max = cam:toWorld(vec(0,0)), cam:toWorld(vec(love.graphics.getDimensions()))
    if oldPos.x > min.x and oldPos.y > min.y and oldPos.x < max.x and oldPos.y < max.y then
      for t=1,steps do
        local vel2 = vel * t / steps
        local dt2 = dt / steps
        bullet_fly_particle(entity.particleBunch, world, oldPos + vel2, (vel.length / dt) / 32, dt2)
      end
    end
  end
  entity.velocity = finalVel
end

function bullet_system:drawBullets(world, entity)
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(assets.images.plasma, entity.position.x, entity.position.y, entity.velocity.angle + math.pi / 2, 1, 1, 15 / 2, 47 / 2)
end

return bullet_system
