local class = require("lib.30log")
local vec = require("lib.brinevector")
local aabb = require("util.aabb")
local ecs = require("ecs")
local assets=require("assets")

local SECTOR_SIZE = 100

local collision_system = class("collision_system")
collision_system.filter = ecs.requireAll("aabb")

function collision_system:preCollisionTest(world, entity, dt)
  entity.collision = false

  local bb = entity.aabb
  world.resources.sectors = world.resources.sectors or {}
  local sectors = world.resources.sectors
  entity.occupiedSectors = entity.occupiedSectors or {}
  local minSX, maxSX = math.floor(bb.x1 / SECTOR_SIZE), math.ceil(bb.x2 / SECTOR_SIZE) - 1
  local minSY, maxSY = math.floor(bb.y1 / SECTOR_SIZE), math.ceil(bb.y2 / SECTOR_SIZE) - 1
  for sx = minSX, maxSX do
    sectors[sx] = sectors[sx] or {}
    entity.occupiedSectors[sx] = entity.occupiedSectors[sx] or {}
    for sy = minSY, maxSY do
      sectors[sx][sy] = sectors[sx][sy] or {isDebugable=true}
      sectors[sx][sy][entity] = true
      entity.occupiedSectors[sx][sy] = true
    end
  end
  for sx, tx in pairs(entity.occupiedSectors) do
    for sy, v in pairs(tx) do
      if sx < minSX or sx > maxSX or sy < minSY or sy > maxSY then
        if (sectors[sx] and sectors[sx][sy] and sectors[sx][sy][entity]) then
          sectors[sx][sy][entity] = nil
          if not next(sectors[sx][sy]) then
            sectors[sx][sy] = nil
          end
        end
        entity.occupiedSectors[sx][sy] = nil
      end 
    end
    if not next(tx) then
      entity.occupiedSectors[sx] = nil
    end
  end
end

function collision_system:collisionTest(world, entity, dt)
  local bb = entity.aabb
  local sectors = world.resources.sectors
  entity.prevcollisions=entity.collisions or {}
  entity.collisions={}
  entity.collisionCount=0
  local dead_ents = {}
  for sx = math.floor(bb.x1 / SECTOR_SIZE), math.ceil(bb.x2 / SECTOR_SIZE) - 1 do
    sectors[sx] = sectors[sx] or {}
    for sy = math.floor(bb.y1 / SECTOR_SIZE), math.ceil(bb.y2 / SECTOR_SIZE) - 1 do
      sectors[sx][sy] = sectors[sx][sy] or {}
      for other, b in pairs(sectors[sx][sy]) do
        if type(other) ~= "string" and other == world:getEntity(other.id) then
          assert(b, "B is "..tostring(b))
          if other ~= entity and type(other)~="string"
            and (not entity.collisions[other])
            and (not entity.nocollide or not entity.nocollide(other))
            and aabb.test(entity.aabb, other.aabb)
          then
            entity.collision = true
            entity.collisions[other]=true
            entity.collisionCount=entity.collisionCount+1
          end
        elseif type(other) ~= "string" and self.filter(other) then
          assert(b, "B is "..tostring(b))
          dead_ents[other] = true
        end
      end
    end
  end
  if entity.occupied_sectors then
    for other in pairs(dead_ents) do
      local bb2 = other.aabb
      for sx, tx in pairs(entity.occupiedSectors) do
        if sectors[sx] then
          for sy, v in pairs(tx) do
            if sectors[sx][sy] then
              sectors[sx][sy][other] = nil
              if not next(sectors[sx][sy]) then
                sectors[sx][sy] = nil
              end
            end
          end
          if not next(sectors[sx]) then
            sectors[sx] = nil
          end
        end
      end
    end
  end
end

function collision_system:drawDebug(world, entity)
  if not entity.collision_dontdebug then
    if not world.resources.sectors then
      return
    end
    if entity.collision then
      love.graphics.setColor(0.2, 0.7, 0.2)
    else
      love.graphics.setColor(0.7, 0.2, 0.2)
    end

    local bb = entity.aabb
    love.graphics.rectangle("line", bb.x1, bb.y1, bb.x2 - bb.x1, bb.y2 - bb.y1)
    love.graphics.setFont(assets.fonts.debug)
    love.graphics.print((entity.collisionCount or -1)..(entity.hp and ";HP=" .. math.floor(entity.hp + .5) or ""), bb.x1, bb.y1)
    local sectors=world.resources.sectors
    love.graphics.setColor(0.1, 0.2, 0.7, 0.1)
    for sx = math.floor(bb.x1 / SECTOR_SIZE), math.ceil(bb.x2 / SECTOR_SIZE) - 1 do
      for sy = math.floor(bb.y1 / SECTOR_SIZE), math.ceil(bb.y2 / SECTOR_SIZE) - 1 do
        if sectors[sx] and sectors[sx][sy] and sectors[sx][sy].isDebugable then
          local entity_count=0
          for k,v in pairs(sectors[sx][sy]) do
            if type(k)~="string" then
              entity_count=entity_count+1
            end
          end
          love.graphics.push("all")
          love.graphics.setColor(1,0,0,1)
          love.graphics.print(entity_count, sx * SECTOR_SIZE, sy * SECTOR_SIZE)
          love.graphics.pop()
          sectors[sx][sy].isDebugable=false
        end
        love.graphics.rectangle("line", sx * SECTOR_SIZE, sy * SECTOR_SIZE, SECTOR_SIZE, SECTOR_SIZE)
      end
    end
  end
end

return collision_system
