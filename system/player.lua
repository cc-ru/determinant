local class = require("lib.30log")
local vec = require("lib.brinevector")
local aabb = require("util.aabb")
local ecs = require("ecs")
local assets = require("assets")

local player_system = class("player_system")
player_system.filter = ecs.requireAll("player", "position", "speed", "direction", "hp", "maxhp", "velocity", "currentWeapon", "weapons", "shake")
local function sign(x)
  return x > 0 and (1) or (x < 0 and -1 or 0)
end
local function death_particle(world,pos,age)
  for n=1,50 do
    world:addEntity({
      particle="raising_circle",
      position=pos+(vec(math.random()*2-1,math.random()*2-1).normalized*math.random()*32)*2,
      lifetime=0.1+math.random()*0.4,
      age=age or 0,
      params={
        ringWidth_start=10*2,
        ringWidth_final=math.random(10,20)*2,
        circleRadius_start=10*2,
        circleRadius_final=math.random(32,40)*2,
        ringColor_start=math.random(2)==1 and 
        {math.random()*0.5+0.5,math.random()*0.6,math.random()*0.01,1} or
        {math.random()*0.5+0.5,math.random()*0.1,math.random()*0.01,1},
        ringColor_final={math.random()*0.1+0.1,math.random()*0.15,math.random()*0.001,0}
      }
    })
  end
end
local function shot_particle(world, pos, rate, direction, entity)
  local t = {
    particle = "bunch",
    position = vec(0,0),
    lifetime = -1,
    age = -1,
    save = false,
    params = {
      particles = {}
    }
  }
  local function add(e)
    table.insert(t.params.particles, e)
  end
  for n=1,10 do
    local radius = math.random(40, 80)
    local pos2 = pos-vec.rotated(vec(30,0),direction)
    add({
      particle="raising_image",
      renderLayer = "smoke",
      position=pos2,
      velocity=vec.rotated(vec(0, math.random(0, 320)), direction-math.pi/2 + math.random()*1-0.5),
      lifetime=0.5 + (math.random()*2-1) * 0.1,
      age=0,
      params={
        imageScale_start = 0,
        imageScale_final = radius/80,
        imageRotation = math.random() * 2 * math.pi,
        image = assets.images.smoke,
        imageColor_start={0,0,0,0.8*math.min(rate/0.05,1/0.8)},
        imageColor_final={0.5,0.5,0.5,0}
      },
      aabb = aabb(pos2.x - 30, pos2.y - 30, pos2.x + 30, pos2.y + 30),
      collision_dontdebug = true
    })
  end
  for n=1,20 do
    add({
      particle="raising_circle",
      renderLayer = "fire",
      position=pos + vec.rotated(vec(0, math.random(0, 16)), math.pi * math.random() * 2),
      lifetime=0.1 + (math.random()*2-1) * 0.05,
      velocityParent = entity,
      age=0,
      params={
        ringWidth_start=5,
        ringWidth_final=40,
        circleRadius_start=0,
        circleRadius_final=math.random(40,50),
        ringColor_start={1+math.random()*0.1-0.05,math.random()*0.5+math.random()*0.1-0.05+math.random()*0.1-0.05,0,math.random()*0.8*math.min(rate/0.1,1)},
        ringColor_final={1,math.random(),0,0}
      }
    })
  end
  world:addEntity(t)
end
function player_system:updatePlayer(world, entity, dt, id, keyfunc)-- keyfunc нужен будет для мультиплеера
  if id~=entity.player then
    return
  elseif id==nil then
    print("WARN: player id is nil, pls fix it")
  end
  local vel = vec(0, 0)

  if keyfunc and keyfunc(entity,"left") or (not keyfunc and love.keyboard.isDown("a")) then
    vel = vel + vec(-1, 0)
  end

  if keyfunc and keyfunc(entity,"right") or  (not keyfunc and love.keyboard.isDown("d")) then
    vel = vel + vec(1, 0)
  end

  if keyfunc and keyfunc(entity,"up") or (not keyfunc and love.keyboard.isDown("w")) then
    vel = vel + vec(0, -1)
  end

  if keyfunc and keyfunc(entity,"down") or (not keyfunc and love.keyboard.isDown("s")) then
    vel = vel + vec(0, 1)
  end
  
  if keyfunc and keyfunc(entity,"regular_gun") or (not keyfunc and love.keyboard.isDown("1")) then
    entity.currentWeapon = entity.weapons.regular
    entity.shotTime = entity.shotTime % entity.currentWeapon.fireRate
  end
  if keyfunc and keyfunc(entity,"minigun_gun") or (not keyfunc and love.keyboard.isDown("2")) then
    entity.currentWeapon = entity.weapons.minigun
    entity.shotTime = entity.shotTime % entity.currentWeapon.fireRate
  end
  if keyfunc and keyfunc(entity,"sniper_gun") or (not keyfunc and love.keyboard.isDown("3")) then
    entity.currentWeapon = entity.weapons.sniper
    entity.shotTime = entity.shotTime % entity.currentWeapon.fireRate
  end
  if keyfunc and keyfunc(entity,"powerbank") or (not keyfunc and love.keyboard.isDown("4")) then
    entity.currentWeapon = entity.weapons.powerbank
    entity.shotTime = entity.shotTime % entity.currentWeapon.fireRate
  end

  entity.velocity = vel.normalized * entity.speed
  entity.shake = math.max(entity.shake - dt * math.max(entity.shake * math.max(entity.shake / 16, 1) * 2, 2), 0)
  
  if entity.player == true then -- Если игрок локальный, двигаем камеру. Важно чтобы было 'if entity.player == true then' а не 'if entity.player then!', т.к. для локального игрока entity.player равен true, для сетевых же он строковый
    world.resources.camera:setPosition(entity.position + vec.rotated(vec(math.random() * entity.shake, 0), math.random() * math.pi * 2))
  end

  local target = world.resources.camera:toWorld(vec(love.mouse.getPosition()))
  entity.direction = (target - entity.position).normalized
  local isShotting = keyfunc and keyfunc(entity,"shot") or (not keyfunc and love.mouse.isDown(1))
  if love.mouse.isDown(2) then -- ЧИИИИИТТТТТТТИЩЕ
    local shift = target - entity.position
    entity.position = entity.position + shift
    if not entity.aabb then
      entity.aabb = aabb(entity.position.x - 30, entity.position.y - 30, entity.position.x + 30, entity.position.y + 30)
    else
      aabb.move(entity.aabb, shift)
    end
  end
  if isShotting then
    entity.shotTime = (entity.shotTime or 0) + dt
  else
    entity.shotTime = math.max(math.min((entity.shotTime or 0) + dt, entity.currentWeapon.fireRate), entity.shotTime or 0)
  end
  local shot_shift = vec(0,0)
  if isShotting and entity.shotTime > entity.currentWeapon.fireRate then
    local count = math.floor(entity.shotTime / entity.currentWeapon.fireRate)
    if (not entity.lastShot) or entity.lastShot+0.05 <= love.timer.getTime() then
      entity.currentWeapon.fireSFX:stop()
      entity.currentWeapon.fireSFX:setPitch(1+math.random()*0.2)
      entity.currentWeapon.fireSFX:setVolume(math.min(entity.currentWeapon.fireRate*8,1)^0.8)
      love.audio.play(entity.currentWeapon.fireSFX)
      entity.lastShot = love.timer.getTime()
    end
    for i = 1, count do
      shot_shift = shot_shift + -entity.direction*(entity.currentWeapon.feedback)
      local vel = vec.rotated(entity.direction, (math.random()-0.5) * entity.currentWeapon.fireAngle) * ((math.random() * 2 - 1) * entity.currentWeapon.fireSpeedRandom*entity.currentWeapon.fireSpeed + entity.currentWeapon.fireSpeed)
      local pos = entity.position + vec.rotated(vec(12, -36), entity.direction.angle + math.pi / 2)
      world:addEntity({
        hit_transparency = entity.currentWeapon.fireRate,
        bullet = true,
        position = pos,
        velocity = vel,
        thrower_velocity=entity.velocity,
        lifetime = entity.currentWeapon.fireLifetime,
        age = 0,
        power=entity.currentWeapon.firePower,
        loss=entity.currentWeapon.fireLoss,
        aabb = aabb(pos.x - 8, pos.y - 8, pos.x + 8, pos.y + 8)
      })
      shot_particle(world, pos, entity.currentWeapon.fireRate, entity.direction.angle, entity)
    end

    entity.shotTime = entity.shotTime % entity.currentWeapon.fireRate
  end
  entity.shot_shift = entity.shot_shift or vec(0,0)
  entity.shot_shift = (entity.shot_shift + shot_shift)

  entity.velocity = entity.velocity + entity.shot_shift
  local moveSteps = 10
  for n = 1, moveSteps do
    entity.position = entity.position + entity.velocity * (dt / moveSteps)
    if not entity.aabb then
      entity.aabb = aabb(entity.position.x - 24, entity.position.y - 24, entity.position.x + 24, entity.position.y + 24)
    else
      aabb.move(entity.aabb, entity.velocity * (dt / moveSteps))
    end
    local eshift = vec(0, 0)
    world:getSystem("collision"):preCollisionTest(world, entity)
    world:getSystem("collision"):collisionTest(world, entity)
    if entity.collisions and entity.collision then
      for other, b in pairs(entity.collisions) do
        if world:getSystem("wall").filter(other) then
          local wpos = vec((other.aabb.x1 + other.aabb.x2) / 2, (other.aabb.y1 + other.aabb.y2) / 2)
          local epos = vec((entity.aabb.x1 + entity.aabb.x2) / 2, (entity.aabb.y1 + entity.aabb.y2) / 2) + eshift
          local wsize = vec(math.abs(other.aabb.x1 - other.aabb.x2), math.abs(other.aabb.y1 - other.aabb.y2))
          local esize = vec(math.abs(entity.aabb.x1 - entity.aabb.x2), math.abs(entity.aabb.y1 - entity.aabb.y2))
          local dist_vec = epos - wpos
          local needed_dist = vec((wsize.x + esize.x) * sign(dist_vec.x), (wsize.y + esize.y) * sign(dist_vec.y)) / 2
          if needed_dist.length > dist_vec.length then
            if math.abs(dist_vec.x) < math.abs(needed_dist.x) and math.abs(dist_vec.y) < math.abs(needed_dist.y) then
              if math.max(math.abs(dist_vec.x), math.abs(dist_vec.y)) == math.abs(dist_vec.x) then
                eshift = eshift + vec(needed_dist.x - dist_vec.x, 0)
                entity.velocity.x = 0
                entity.shot_shift.x = 0
              else
                eshift = eshift + vec(0, needed_dist.y - dist_vec.y)
                entity.velocity.y = 0
                entity.shot_shift.y = 0
              end
            end
          end
        end
      end
    end
    
    entity.position = entity.position + eshift
    aabb.move(entity.aabb, eshift)
  end
  
  entity.shot_shift = entity.shot_shift-(entity.shot_shift*dt*8)
  entity.hp_bar_transparency = entity.hp_bar_transparency or 0
  if entity.hp < entity.maxhp then
    entity.hp_bar_transparency = 1
  else
    entity.hp_bar_transparency = math.max(entity.hp_bar_transparency-dt*0.5, 0)
  end
  if entity.hp <= 0 then
    death_particle(world,entity.position)
    world:removeEntity(entity.id)
  end
end
function player_system:drawPlayer(world, entity, overlays)
  -- Рисуем индикатор здоровья 
  if overlays then
    if entity.hp_bar_transparency then
      love.graphics.setColor(0.8, 0.2, 0.2, entity.hp_bar_transparency)
      love.graphics.rectangle("fill", entity.position.x - 60, entity.position.y - 60 - 10, 120, 10)
      love.graphics.setColor(0.2, 0.8, 0.2, entity.hp_bar_transparency)
      love.graphics.rectangle("fill", entity.position.x - 60, entity.position.y - 60 - 10, 120 * entity.hp / entity.maxhp, 10)
    end
    -- рисуем прицел
    if entity.player == true then -- Отрисовываем прицел только если игрок локальный
      local target = world.resources.camera:toWorld(vec(love.mouse.getPosition()))
      love.graphics.setColor(1.0, 1.0, 1.0)
      love.graphics.circle("line", target.x, target.y - 1, 10)
      love.graphics.setColor(1.0, 0, 0)
      love.graphics.circle("line", target.x, target.y, 10)
    end
  else
    love.graphics.setColor(1.0, 1.0, 1.0)
    
    -- рисуем игрока с тенью
    love.graphics.draw(
        assets.images.shadow,
        entity.position.x, entity.position.y + 16,
        0,
        1.0, 1.0, 64, 64
    )
    love.graphics.draw(
        assets.images.player,
        entity.position.x, entity.position.y,
        entity.direction.length > 0 and entity.direction.angle  + math.pi / 2 or math.random() * math.pi * 2,
        0.6, 0.6, 64, 92
    )
    
  end
end

return player_system
