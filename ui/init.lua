local ui = {}

local function call(t, method, ...)
  for _, element in pairs(t) do
    if element[method] then
      element[method](element, ...)
    end
  end
end

local methods = {
  "draw", "keypressed", "keyreleased", "mousemoved", "mousepressed", "mousereleased", "textedited",
  "textinput", "touchmoved", "update", "wheelmoved"
}

for _, method in ipairs(methods) do
  ui[method] = function(t, ...)
    call(t, method, ...)
  end
end

ui.menu_button = require("ui.menu_button")

return ui
