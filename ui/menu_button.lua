local class = require("lib.30log")
local assets = require("assets")

local menu_button = class("menu_button")

function menu_button:init(text, x, y, w, h)
  self.x = x
  self.y = y
  self.w = w
  self.h = h
  self.text = text
  self.t = 0
end

function menu_button:draw()
  local font = assets.fonts.menu
  local textWidth, textHeight = font:getWidth(self.text), font:getHeight()

  love.graphics.setFont(font)
  love.graphics.setColor(0.05, 0.05, 0.05)

  local x, y = self.x + self.w / 2 - textWidth / 2, self.y + self.h / 2 - textHeight / 2

  love.graphics.print(self.text, x, y)

  love.graphics.setColor(0.05, 0.05, 0.05, self.t)
  love.graphics.print(self.text, x + 0.1, y + 0.1)
  love.graphics.print(self.text, x + 0.1, y - 0.1)
  love.graphics.print(self.text, x - 0.1, y + 0.1)
  love.graphics.print(self.text, x - 0.1, y - 0.1)

  love.graphics.setColor(0.05, 0.05, 0.05)
  love.graphics.rectangle("fill", self.x + (1 - self.t) * self.w, self.y, self.t * self.w, 2)
  love.graphics.rectangle("fill", self.x, self.y + self.h - 1, self.t * self.w, 2)
end

function menu_button:contains(x, y)
  return x >= self.x and y >= self.y and x <= (self.x + self.w) and y <= (self.y + self.h)
end

function menu_button:update(dt)
  self.t = math.min(1.0, math.max(0.0, self.t + dt * (self.hovered and 1 or -1) / 0.2))
end

function menu_button:mousereleased(x, y)
  if self:contains(x, y) and self.clicked then
    self:clicked()
  end
end

function menu_button:mousemoved(x, y)
  if self:contains(x, y) then
    if not self.hovered then
      love.mouse.setCursor(love.mouse.getSystemCursor("hand"))
      self.hovered = true
      self.t = 0
    end
  elseif self.hovered then
    self.hovered = false
    love.mouse.setCursor(love.mouse.getSystemCursor("arrow"))
  end
end

return menu_button
