local state = {stack = {}}

local callbacks = {
  "draw", "focus", "keypressed", "keyreleased", "mousefocus", "mousemoved", "mousepressed",
  "mousereleased", "quit", "resize", "textedited", "textinput", "threaderror", "touchmoved",
  "touchpressed", "touchreleased", "update", "visible", "wheelmoved"
}

local function currentState()
  return state.stack[#state.stack]
end

local function callState(st, method, ...)
  for _, layer in ipairs(st) do
    if layer[method] then
      layer[method](...)
    end
  end
end

function state:registerCallbacks()
  for _, callback in ipairs(callbacks) do
    love[callback] = function(...)
      local current = currentState()
      if callback == "keypressed" then
        local k = ...
        if k == "f11" then
          love.window.setFullscreen(not love.window.getFullscreen(), "desktop")
        end
      end
      if not current then return end
      callState(current, callback, ...)
    end
  end
end

function state.push(...)
  if currentState() then
    callState(currentState(), "paused")
  end

  table.insert(state.stack, {...})
  callState(currentState(), "started")
end

function state.pop()
  callState(currentState(), "stopped")
  table.remove(state.stack)

  if currentState() then
    callState(currentState(), "resumed")
  end
end

function state.replace(...)
  callState(currentState(), "stopped")
  state.stack[#state.stack] = {...}
  callState(currentState(), "started")
end

return state
