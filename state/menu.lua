local assets = require("assets")
local ui = require("ui")
local state = require("state")
local game = require("state.game")

local menu = {
  ui = {},
  intro = true,
  introTimer = 0,
  introLifetime = 4,
  fade = -1,  -- изменяется от -1 до 1, на 0 - смена intro
}

local function buildUI()
  local width, height = love.graphics.getDimensions()

  local entries = {
    {"singleplayer", "ОДИНОЧНАЯ ИГРА"},
    {"multiplayer", "СЕТЕВАЯ ИГРА"},
    {"settings", "НАСТРОЙКИ"},
    {"quit", "ВЫХОД"}
  }

  local menuOffsetX = 64
  local menuOffsetY = 64
  local menuItemSeparator = 16

  -- расставляем кнопки меню
  local x = menuOffsetX
  local fontHeight = assets.fonts.menu:getHeight()
  for _, entry in ipairs(entries) do
    local entryWidth = assets.fonts.menu:getWidth(entry[2])
    menu.ui[entry[1]] = ui.menu_button(entry[2], x, height - menuOffsetY, entryWidth, fontHeight)
    x = x + entryWidth + menuItemSeparator
  end

  function menu.ui.singleplayer:clicked()
    ui.mousemoved(menu.ui, 0, 0)
    state.replace(game)
  end

  function menu.ui.quit:clicked()
    love.event.quit()
  end
end

function menu.started()
  local width, height = love.graphics.getDimensions()

  menu.background = love.graphics.newQuad(0, 0, width, height + 32, assets.images.background.pattern:getDimensions())
  menu.backgroundOffset = 0

  -- готовим отдельный канвас для лого
  local teamWidth, teamHeight = assets.images.team:getDimensions()
  local canvasWidth, canvasHeight = 500, 700
  menu.canvas = love.graphics.newCanvas(canvasWidth, canvasHeight)
  love.graphics.setCanvas(menu.canvas)
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.draw(assets.images.team, canvasWidth / 2, canvasHeight / 2, 0, 0.6, 0.6, teamWidth / 2, teamHeight / 2.4)
  love.graphics.setCanvas()

  -- параметры хроматической аберрации на лого
  menu.magnitude = 0.0
  menu.angle = 0.0

  -- инициализируем остальной интерфейс меню
  buildUI()

  -- готовим данные для дискорда
  if DISCORDRPC == true then
    gamestate = "menu"
    discord_update()
  end
end

function menu.keepMusicOn()
  if menu.currentMusicTrack == nil or not menu.currentMusicTrack:isPlaying() then
    menu.currentMusicTrack = assets.music.menu.random
    love.audio.play(menu.currentMusicTrack)
  end
end

function menu.stopped()
  love.audio.stop()
end

function menu.mousemoved(...)
  ui.mousemoved(menu.ui, ...)
end

function menu.mousereleased(...)
  ui.mousereleased(menu.ui, ...)
end

function menu.keypressed(key)
  if key == "escape" or key == "q" then
    love.event.quit()
  elseif key == "space" then
    love.audio.stop()
    menu.keepMusicOn()
  end
end

function menu.update(dt)
  if menu.intro then
    menu.introTimer = menu.introTimer + dt
    menu.intro = menu.introTimer < menu.introLifetime
    -- меняем интро на лого
    if not menu.intro then
      local canvasWidth, canvasHeight = menu.canvas:getDimensions()
      local logoWidth, logoHeight = assets.images.logo:getDimensions()
      love.graphics.setCanvas(menu.canvas)
      love.graphics.setColor(1, 1, 1, 1)
      love.graphics.clear()
      love.graphics.draw(assets.images.logo, canvasWidth / 2, canvasHeight / 2, 0, 0.9, 0.9, logoWidth / 2, logoHeight / 2)
      love.graphics.setCanvas()
    end
  else
    -- обновляем интерфейс
    ui.update(menu.ui, dt)
  end

  -- меняем параметр плавного перехода
  if menu.fade < 1.0 and menu.introTimer > menu.introLifetime - 1 then menu.fade = menu.fade + dt end

  -- следим за музыкой, обновляем анимации и спецэффекты
  menu.keepMusicOn()
  menu.backgroundOffset = (menu.backgroundOffset + dt * 20) % 32
  assets.fx.smoke:update(dt)

  -- обновляем хроматическую аберрацию
  if menu.magnitude < 0.0001 then
    menu.magnitude = 0

    if math.random(0, 100) > 98 then
      menu.magnitude = math.random() * 0.005 + 0.001
      menu.angle = math.random() * 2 * math.pi
    end
  else
    if math.random(0, 100) > 90 then
      menu.magnitude = math.random() * 0.005 + 0.001
    else
      menu.magnitude = math.max(0.0, menu.magnitude * 0.9)
    end
  end
end

function menu.draw()
  local width, height = love.graphics.getDimensions()

  -- рисуем фон и дым
  love.graphics.setBackgroundColor(1, 1, 1, 1)
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.draw(assets.images.background.shadow, 0, 0, 0, width / 32, height / 32)
  love.graphics.draw(assets.images.background.pattern, menu.background, 0, menu.backgroundOffset - 32)
  love.graphics.draw(assets.fx.smoke, width / 2, height + 128)

  -- подготавливаем шейдер для отрисовки лого
  local s = assets.shaders.chromaticAberration
  s:send("angle", menu.angle)
  s:send("magnitude", menu.magnitude)

  -- рисуем лого
  love.graphics.setShader(s)
  love.graphics.setColor(1, 1, 1, math.abs(menu.fade))
  love.graphics.draw(menu.canvas, width / 2 - menu.canvas:getWidth() / 2, (height - 84) / 2 - menu.canvas:getHeight() / 2)
  love.graphics.setShader()

  if not menu.intro then
    -- версия в углу
    love.graphics.setFont(assets.fonts.menu)
    love.graphics.setColor(0.01, 0.01, 0.01)
    love.graphics.print("v" .. VERSION, width - 64 - assets.fonts.menu:getWidth("v" .. VERSION), height - 64)

    -- интерфейс
    ui.draw(menu.ui)
  end
end

function menu.resize(width, height)
  menu.background = love.graphics.newQuad(0, 0, width, height + 32, assets.images.background.pattern:getDimensions())
  assets.fx.smoke:setEmissionArea("uniform", width / 2, 0)
  assets.fx.smoke:setEmissionRate(width / 170)
  buildUI()
end

return menu
