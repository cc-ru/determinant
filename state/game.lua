local vec = require("lib.brinevector")
local ecs = require("ecs")
local camera = require("util.camera")
local debug = require("util.debug")
local assets = require("assets")

local game = {
  currentMusicTrack = nil,
}

function game.newEnemy(pos)
  game.world:addEntity({
    enemy=true,
    aabb = require("util.aabb")(pos.x-45,pos.y-45,pos.x+45,pos.y+45),
    position=pos,
    hp=40,
    maxhp=40,
    speed=math.random(250,320),
    direction=0,
    onDeath = function()
      game.enemyRate = game.enemyRate - 0.1*math.min(1,game.enemyRate)
    end,
  })
end

function game.keepMusicOn()
  if game.currentMusicTrack == nil or not game.currentMusicTrack:isPlaying() then
    game.currentMusicTrack = assets.music.action.random
    love.audio.play(game.currentMusicTrack)
  end
end

function game.started(isStarted)
  game.world = ecs.world()
  game.world.resources.camera = camera(0.7)
  game.world:addSystem("player", require("system.player"))
  game.world:addSystem("bullet", require("system.bullet"))
  game.world:addSystem("collision", require("system.collision"))
  game.world:addSystem("enemy", require("system.enemy"))
  game.world:addSystem("particle", require("system.particle"))
  game.world:addSystem("item", require("system.item"))
  game.world:addSystem("wall", require("system.wall"))
  do
    local player={
      player = true,
      position = vec(10, 10),
      direction = vec(1, 0),
      speed = 300,
      shake = 0,
      weapons = {
        powerbank = {
          feedback = -2000,
          fireRate = math.huge,
          fireAngle = 0,
          fireSpeed = 10000,
          fireSpeedRandom = 0,
          firePower = 100,
          fireLoss = 100,
          fireLifetime = 1,
          fireSFX = assets.sfx.shot:clone()
        },
        regular = {
          feedback = 500,
          fireRate = 0.5,
          fireAngle = 0.1,
          fireSpeed = 700,
          fireSpeedRandom = 0.1,
          firePower = 5,
          fireLoss = 5,
          fireLifetime = 1,
          fireSFX = assets.sfx.shot:clone()
        },
        minigun = {
          feedback = 50,
          fireRate = 0.05,
          fireAngle = 1,
          fireSpeed = 1000,
          fireSpeedRandom = 0.1,
          firePower = 5*(0.05/0.5),
          fireLoss = 5*(0.05/0.5),
          fireLifetime = 0.6,
          fireSFX = assets.sfx.shot:clone()
        },
        sniper = {
          feedback = 1000,
          fireRate = 1,
          fireAngle = 0.03,
          fireSpeed = 4000,
          fireSpeedRandom = 0.1,
          firePower = 5*(1/0.5),
          fireLoss = 5*(1/0.5),
          fireLifetime = 2,
          fireSFX = assets.sfx.shot:clone()
        }
      },
      hp = 100,
      maxhp = 100,
      velocity = vec(0,0)
    }
    player.currentWeapon = player.weapons.regular
    game.world:addEntity(player)
  end
  game.world:addEntity({
    collisionCleaner=true
  })
  for x = -5,5 do
    for y = -5,5 do
      if not (x == -1 and y == 5) and ((x == -5 or x == 5) or (y == -5 or y == 5)) then
        game.world:addEntity({
          wall = "stretchTexture",
          aabb = require("util.aabb")(x*100, y*100, x*100+100, y*100+100),
          params = {
            texture = assets.images.wall
          }
        })
      end
    end
  end

  game.backgroundQuad = love.graphics.newQuad(0, 0, 10000, 10000, assets.images.ground:getDimensions())

  game.enemyRate = 10
  game.toGameRestart = false
  game.toNextEnemy = 0
  game.paused = false
  if not isStarted then
    game.debug = {}
    game.debug.visible = false
    game.debug.fps = debug.graph(10, 10, "FPS", "%.1f")
    game.debug.memory = debug.graph(10, 72, "Lua memory (K)", "%.1f")
    game.debug.entities = debug.graph(10, 134, "Entities", "%d")
    game.debug.drawcalls = debug.graph(190, 10, "Draw calls", "%d")
    game.debug.texturememory = debug.graph(190, 72, "Texture memory (K)", "%.1f")
    game.debug.batched = debug.graph(190, 134, "Calls batched", "%d")
    game.debug.lastStats = love.graphics.getStats()
  end
  if DISCORDRPC == true then
    gamestate = "game"
    discord_update()
  end
end

function game.stopped()
  love.audio.stop()
end

function game.update(dt)
  game.keepMusicOn()
  game.world.resources.camera.scale = 0.8 / (720 / math.min(love.graphics.getHeight(), love.graphics.getWidth()))
  if not game.paused then
    --game.world:fire("preCollisionTest", dt)
    --game.world:fire("collisionTest", dt) -- 
    game.world:fire("update", dt)
    game.world:fire("updatePlayer", dt, true)
    game.toNextEnemy=game.toNextEnemy-dt
    if not game.world:select(game.world:getSystem("player").filter)() then
      game.toGameRestart=game.toGameRestart or 5
      game.toGameRestart=game.toGameRestart-dt
      if game.toGameRestart <= 0 then
        game.started(true)
      end
    else
      if game.toNextEnemy<=0 then
        game.toNextEnemy=game.toNextEnemy+game.enemyRate
        print(math.floor(game.enemyRate*100)/100)
        game.newEnemy(game.world:select(game.world:getSystem("player").filter)().position+vec(math.random(2)==1 and math.random(-2000,-1000) or math.random(1000,2000),math.random(2)==1 and math.random(-2000,-1000) or math.random(1000,2000)))
      end
    end
  end

  game.debug.fps:update(dt, love.timer.getFPS())
  game.debug.memory:update(dt, collectgarbage("count"))
  game.debug.entities:update(dt, game.world.entitiesCount)
  game.debug.drawcalls:update(dt, game.debug.lastStats.drawcalls)
  game.debug.texturememory:update(dt, game.debug.lastStats.texturememory / 1024)
  game.debug.batched:update(dt, game.debug.lastStats.drawcallsbatched)
  game.toCollect = (game.toCollect or 0) + dt
  if game.toCollect > 1 then
    game.toCollect = game.toCollect - 1
    collectgarbage("collect")
  end
end

function game.keypressed(key)
  if key == "escape" then
    require("state").replace(require("state.menu"))
  elseif key == "f1" then
    game.debug.visible = not game.debug.visible
  elseif key == "space" then
    game.paused = not game.paused
  end
end

function game.draw()
  local width, height = love.graphics.getDimensions()

  game.world.resources.camera:draw(function (camera)
    love.graphics.setColor(1, 1, 1)
    do
      local pos = camera:getPosition()
      local w,h = love.graphics.getDimensions()
      local gpos = pos-vec(w/camera.scale/2,h/camera.scale/2)
      local gpos2 = pos+vec(w/camera.scale/2,h/camera.scale/2)
      local iw,ih = assets.images.ground:getWidth(), assets.images.ground:getHeight()
      local ux,uy = gpos.x % iw, gpos.y % ih
      game.backgroundQuad:setViewport(ux, uy, gpos2.x - gpos.x, gpos2.y - gpos.y)
      love.graphics.draw(assets.images.ground, game.backgroundQuad, gpos.x, gpos.y)
    end
    game.world:fire("drawItems")
    game.world:fire("drawParticles", "bullet_fly")
    game.world:fire("drawParticles", "smoke")
    game.world:fire("drawEnemies")
    game.world:fire("drawBullets")
    game.world:fire("drawPlayer")
    game.world:fire("drawParticles")
    game.world:fire("drawParticles", "fire")
    game.world:fire("drawWalls")
    game.world:fire("drawEnemies", true)
    game.world:fire("drawPlayer", true)
    
    if game.debug.visible then
      game.world:fire("drawDebug")
    end
  end)

  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(assets.images.background.shadow, 0, 0, 0, width / 32, height / 32)

  if game.paused then
    love.graphics.push("all")
    love.graphics.setColor(0, 0, 0, 0.4)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getDimensions())
    love.graphics.pop()
    if DISCORDRPC == true then
      gamestate = "game-paused"
      discord_update()
    end
  end

  if game.debug.visible then
    game.debug.fps:draw()
    game.debug.memory:draw()
    game.debug.entities:draw()
    game.debug.drawcalls:draw()
    game.debug.texturememory:draw()
    game.debug.batched:draw()
  end

  game.debug.lastStats = love.graphics.getStats()
end

return game
