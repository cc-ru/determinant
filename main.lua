require("const")
require("lib.getArch")

local assets = require("assets")
local state = require("state")
local menu = require("state.menu")

function love.load()
  
  -- TODO: добавить реальную поддержку Android если это возможно
  if love.system.getOS() == "Android" then
    function love.draw()
      text = love.graphics.print("The game cannot be run on mobile devices.", 10)
    end
    return 0
  end
  love.window.setTitle("determinant " .. VERSION)
  love.window.setMode(1024, 600, { resizable = true, minwidth = 400, minheight = 300 })

  if DISCORDRPC == true then
    discord = require("lib.discord")
    discord_load()
  end

  assets.load()

  state.registerCallbacks()
  state.push(menu)
end
