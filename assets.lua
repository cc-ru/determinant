-- TODO: переделать так, чтобы ассеты сами рекурсивно загружались из папки assets/

local assets = {
  images = {},
  fonts = {},
  music = {},
  fx = {},
  shaders = {}
}

local function loadImage(name)
  return love.graphics.newImage("assets/images/" .. name .. ".png")
end

local function loadSFX(name)
  return love.audio.newSource("assets/sfx/" .. name, "static")
end

local function loadMusic()
  -- мета функция для получения рандомного трека
  -- пример: assets.music.action.random - рандомный трек из темы action
  local meta = {
    __index = function(tracks, key)
      if key == "random" then
         return tracks[love.math.random(#tracks)]
      else
         return tracks[key]
      end
   end
  }
  -- грузим треки из папки с музыкой, раскидывая по темам
  local lfs = love.filesystem
  local themes = lfs.getDirectoryItems("assets/music/")
  for _, theme in pairs(themes) do
    local info = lfs.getInfo("assets/music/" .. theme)
    if info.type == "directory" then
      assets.music[theme] = {}
      local tracks = lfs.getDirectoryItems("assets/music/" .. theme)
      for _, track in pairs(tracks) do
        table.insert(assets.music[theme], love.audio.newSource("assets/music/" .. theme .. "/" .. track, "stream"))
      end
      setmetatable(assets.music[theme], meta)
    end
  end
end

local function loadFont(filename, size)
  return love.graphics.newFont("assets/fonts/" .. filename, size)
end

function assets.load()
  -- загружаем картинки
  assets.images.logo = loadImage("logo")
  assets.images.team = loadImage("team")
  assets.images.background = {
    pattern = loadImage("background-pattern"),
    shadow = loadImage("background-shadow")
  }
  assets.images.background.pattern:setWrap("repeat")
  assets.images.smoke = loadImage("smoke")
  assets.images.ground = loadImage("ground")
  assets.images.ground:setWrap("repeat")
  assets.images.plasma = loadImage("plasma")
  assets.images.spider = loadImage("spider")
  assets.images.shadow = loadImage("entity-shadow")
  assets.images.player = loadImage("player")
  assets.images.wall = loadImage("simple-wall")
  assets.images.items = {
    fireBounceUpgrade = loadImage("item_fireBounceUpgrade"),
    firePowerUpgrade = loadImage("item_firePowerUpgrade"),
    fireRateUpgrade = loadImage("item_fireRateUpgrade"),
    hp = loadImage("item_hp")
  }
  
  -- загружаем музыку
  loadMusic()
  
  -- загружаем SFX
  assets.sfx = {
    shot = loadSFX("shot.wav"),
    hurt = loadSFX("hurt.wav")
  }
  
  -- генерируем системы частиц
  assets.fx.smoke = love.graphics.newParticleSystem(assets.images.smoke, 100)
  assets.fx.smoke:setParticleLifetime(5, 10)
  assets.fx.smoke:setEmissionArea("uniform", 1024 / 2, 0)
  assets.fx.smoke:setEmissionRate(6)
  assets.fx.smoke:setSizeVariation(1)
  assets.fx.smoke:setRotation(0, 2 * math.pi)
  assets.fx.smoke:setSpin(0.1, 0.2)
  assets.fx.smoke:setSpinVariation(.4)
  assets.fx.smoke:setDirection(5)
  assets.fx.smoke:setSpeed(40, 100)
  assets.fx.smoke:setSizeVariation(0.5)
  assets.fx.smoke:setColors(255, 255, 255, 255, 255, 255, 255, 0)
  
  -- загружаем шейдеры
  assets.shaders.chromaticAberration = love.graphics.newShader("assets/shaders/chromatic-aberration.glsl")
  
  -- загружаем шрифты
  assets.fonts.menu = love.graphics.newFont("assets/fonts/FiraSans-Light.ttf", 20, "normal")
  assets.fonts.debug = love.graphics.newFont("assets/fonts/FiraSans-Light.ttf", 16, "normal")
  assets.fonts.debug_tiny = love.graphics.newFont("assets/fonts/FiraSans-Light.ttf", 12, "normal")
end

return assets
