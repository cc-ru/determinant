getArch = function ()
    if love._os:match('Windows') then
      return os.getenv('PROCESSOR_ARCHITECTURE') -- will return 'x86' or 'x64'
    else
      local str_arch = io.popen('uname -p'):read('*a')
        if str_arch then
          return str_arch
      end
    end
  end
return getArch